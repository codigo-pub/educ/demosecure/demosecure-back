package com.demosecure.demosecure.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserRestController {

    static final Logger LOG = LoggerFactory.getLogger(UserRestController.class);

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    /**
     * Dans un contexte professionnel, on se doit de toujours verifier que la ressource demandee par l'utilisateur
     * lui appartient bien, ou du moins qu'il a le droit d'y accéder !
     * Cela signifie qu'il nous faut deja recuperer les informations sur l'utilisateur.
     *
     * Rappel : Le filtre JwtAuthenticationFilter a pour but de prendre le token, le convertir en objet
     * UsernamePasswordAuthenticationToken et l'injecter dans le contexte de securite spring en amont,
     * afin que spring identifie l'utilisateur.
     *
     * Cette methode, ci-dessous, montre comment on peut recuperer cet objet depuis le controller afin d'utiliser ces informations
     * pour etre sur que la ressource demandee appartient bien a l'utilisateur
     * @return
     */
    @GetMapping("/something/{idsomething}")
    public String doSomethingWithCheckToken(@PathVariable("idsomething") Integer idsomething) {
        //recuperation du contexte de securite propre a l'utilisateur
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        //le getName va nous donner son login. Ce login nous permettra d'aller verifier qu'il a bien acces a la ressource
        //si l'on souhaite mettre le id du user dans le token pour l'utiliser a la place du login, il faudra avoir notre
        // propre UserDetails etendu (via extends) qui amene cette information et donc modifier DemoUserDetailsService
        //en consequence
        LOG.info("On fait quelque chose avec id : [{}] pour le user : [{}]", idsomething, authentication.getName());
        return "dosomething";
    }

}
